#ifndef D_DBDERIVEDBIND_H
#define D_DBDERIVEDBIND_H

#include "storage/RowItem.h"
#include "storage/RowBind.h"
#include "Statute.h"
#include "Entity.h"

//
//--------------------------------------------------------
// DerivedBind - instance one
//--------------------------------------------------------
//
class DBDerivedBind: public Storage::Bind<Storage::RowItem>
{
public:
    DBDerivedBind ( std::vector<Module::Statute>* dest );
    virtual ~DBDerivedBind();
    // doBind - when an item is add or set with data - flow to dest source
    void doBind(const Storage::RowItem &item);
    // doBind - when an item is add or set with data - flow to dest source
    void doBindUpdate(const Storage::RowItem &item, int index);
    // doBindClear - when an item is add or set with data - flow to dest source
    void doBindClear(const Storage::RowItem &item, int index);
    // doUpdate - update an item from dest to source (BindSource items)
    void doUpdate(Storage::RowItem & item, int index);
public:    
    std::vector<Module::Statute> *destSource;

};

class DBDerivedItemBind: public Storage::Bind<Storage::RowItem>
{
public:
    DBDerivedItemBind ( Module::Statute* dest );
    virtual ~DBDerivedItemBind();
    // doBind - when an item is add or set with data - flow to dest source
    void doBind(const Storage::RowItem &item);
    // doBind - when an item is add or set with data - flow to dest source
    void doBindUpdate(const Storage::RowItem &item, int index);
    // doBindClear - when an item is add or set with data - flow to dest source
    void doBindClear(const Storage::RowItem &item, int index);
    // doUpdate - update an item from dest to source (BindSource items)
    void doUpdate(Storage::RowItem & item, int index);
public:    
    Module::Statute *destSource;
    
};


#endif  // D_DBDERIVEDBIND_H
