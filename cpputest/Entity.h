/*
 * Hero.h
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include "storage/EntityMap.h"
#include "storage/DBStorage.h"
#include "Statute.h"
#include <string>


namespace Storage {


/// EntityMap Statue
STORENTMAP_DECLARE_BEGIN(Statute, Module::Statute)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(StatuteID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Title, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(Quota, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(StatuteID, getStatuteID)
GET_STRING_ACCESSOR(Title, getTitle)
GET_INT_ACCESSOR(Quota, getQuota)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(StatuteID, setStatuteID)
SET_STRING_ACCESSOR(Title, setTitle)
SET_INT_ACCESSOR(Quota, setQuota)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END


} /* namespace Storage */


#endif /* ENTITY_H_ */
