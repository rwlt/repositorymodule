/*
 * Hero.h
 */

#ifndef HERO_H_
#define HERO_H_

#include <string>
#include "storage/EntityMap.h"

namespace Module
{


class Statute {
public:
	/// Constructor
	Statute()
	{
	}
	/// Constructor from person
	/**
	 * \param person const reference to person
	 * \param newStatute const string
	 */
	Statute(int newid, const std::string &title):
	     m_StatuteID(newid), m_Title(title)
	{
	}
	/// virtual deconstructor
	virtual ~Statute(){};

	ENTITY_INT_ATTRIBUTE(StatuteID)
	ENTITY_STRING_ATTRIBUTE(Title)
	ENTITY_INT_ATTRIBUTE(Quota)
};

} /* namespace Module */


#endif /* HERO_H_ */
