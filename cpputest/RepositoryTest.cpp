
#include "repository/Repository.h"
#include "Statute.h"
#include "Entity.h"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <string>
#include <sstream>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//-------------------------------------------------------
// Repository Accessor test group
//-------------------------------------------------------
//newstatute

TEST_GROUP(RepositoryAccessor) {
    const std::string ServerName{"localhost"};
	std::string desc{"Module DBStorage 1.0 - September 6, 2016"};
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"StatuteID\" type=\"integer\"/>\n"
    "        <field name=\"Title\" type=\"string\"/>\n"
    "        <field name=\"Quota\" type=\"integer\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "</module-dbstorage>\n"};
    Repository::Repository<std::string> repo{&dbStorage, "Key"};
    Repository::Repository<Module::Statute> repoStatute{&dbStorage, "StatuteID"};
    std::vector<Module::Statute> statutes;
    void setup() {
        dbStorage.loginUser("TEST", "pass");
        CHECK(true == dbStorage.isLoggedUser());

    	int keym = 20;
        for (int index=1;index < 11; ++index) {
        	std::stringstream name;
        	name << "Statute " << (index * keym);
        	statutes.push_back(Module::Statute((index * keym), name.str()));
        }
    }

    void teardown() {
        // Find new items by retrieve where criteria
    	// and clear to finish tests.
        auto result = dbStorage.doRead<Module::Statute>({{"Title", "Pikeman"}}, {});
    	for (Module::Statute statute: result) {
    		dbStorage.doRemoveItem<Module::Statute>(statute);
    	}
        auto result2 = dbStorage.doRead<Module::Statute>({{"Title", "Archer"}}, {});
    	for (Module::Statute statute: result2) {
    		dbStorage.doRemoveItem<Module::Statute>(statute);
    	}
    }
};

TEST(RepositoryAccessor, TrackStateChangesAccessor) {
    LONGS_EQUAL(0, repoStatute.size());

    for (int i = 0; i < 3; ++i) {// 20, 40, 60
		repoStatute.addItem(statutes.at(i).getStatuteID(), statutes.at(i));
    }
    LONGS_EQUAL(3, repoStatute.size());
    STRCMP_EQUAL("Statute 20", repoStatute.getItem(20).getTitle().c_str());

    // Tracking changes means key can be reset with values
    repoStatute.trackStateChanges(true);
    try {
    	repoStatute.addItem(80, Module::Statute(80, "Statute 80"));
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository addItem new pair KeyType and item not allowed under tracking state mode", e.what());
    }
    LONGS_EQUAL(3, repoStatute.size());

    // Repeat add - should be no change they still exist
    for (int i = 0; i < 3; ++i) {// 20, 40, 60
		repoStatute.addItem(statutes.at(i).getStatuteID(), statutes.at(i));
    }
    repoStatute.addItem(40, Module::Statute(40, "New Name for 40"));
    LONGS_EQUAL(3, repoStatute.size());
    STRCMP_EQUAL("New Name for 40", repoStatute.getItem(40).getTitle().c_str());

    // No tracking it is an error to reset values at keys, only new items can be added
    repoStatute.trackStateChanges(false);
    try {
    	repoStatute.addItem(40, Module::Statute(40, "Statute 40"));
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository addItem no duplicate key can be added", e.what());
    }

    // Get list of the items that exist, no removals have been made
    std::vector<Module::Statute> list = repoStatute.getItems();
    LONGS_EQUAL(3, list.size());
    STRCMP_EQUAL("Statute 20", list.at(0).getTitle().c_str());
    STRCMP_EQUAL("New Name for 40", list.at(1).getTitle().c_str());
    STRCMP_EQUAL("Statute 60", list.at(2).getTitle().c_str());

    // Make a removal with no tracking
    try {
    	repoStatute.deleteItem(40);
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository deleteItem available in tracking state mode only", e.what());
    }

    // Start trackStateChanges again
    repoStatute.trackStateChanges(true);

    // Updateitem is used to modify the content
    repoStatute.updateItem(20, Module::Statute(20, "20 20 20 20"));
    list = repoStatute.getItems();
    LONGS_EQUAL(3, list.size());
    LONGS_EQUAL(3, repoStatute.size());
    STRCMP_EQUAL("20 20 20 20", list.at(0).getTitle().c_str());
    STRCMP_EQUAL("New Name for 40", list.at(1).getTitle().c_str());
    STRCMP_EQUAL("Statute 60", list.at(2).getTitle().c_str());

    repoStatute.trackStateChanges(false);
    try {
    	repoStatute.updateItem(40, Module::Statute(40, "40404040"));
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository updateItem available in tracking state mode only", e.what());
    }

    repoStatute.trackStateChanges(true);
    // Update item not in is an exception thrown an error in logic
    try {
    	repoStatute.updateItem(140, Module::Statute(140, "Hi40404040"));
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository updateItem key not found", e.what());
    }
    list = repoStatute.getItems();
    LONGS_EQUAL(3, list.size());
    LONGS_EQUAL(3, repoStatute.size());
    STRCMP_EQUAL("20 20 20 20", list.at(0).getTitle().c_str());
    STRCMP_EQUAL("New Name for 40", list.at(1).getTitle().c_str());
    STRCMP_EQUAL("Statute 60", list.at(2).getTitle().c_str());

}


TEST(RepositoryAccessor, AddItemWithKeyAccessor) {
    for (int i = 0; i < 3; ++i) {// 20, 40, 60
		repoStatute.addItem(statutes.at(i).getStatuteID(), statutes.at(i));
    }
    LONGS_EQUAL(3, repoStatute.size());
    STRCMP_EQUAL("Statute 20", repoStatute.getItem(20).getTitle().c_str());

    // Tracking changes means key can be reset with values
    repoStatute.trackStateChanges(true);
    try {
    	repoStatute.addItem(80, Module::Statute(80, "Statute 80"));
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository addItem new pair KeyType and item not allowed under tracking state mode", e.what());
    }
    LONGS_EQUAL(3, repoStatute.size());

    Module::Statute newstatute(0, "Archer");
    int id = repoStatute.addItem(newstatute);
    auto chkstatute = repoStatute.getItem(id);
    STRCMP_EQUAL("Archer", chkstatute.getTitle().c_str());
    // Get list of the items that exist, no removals have been made
    std::vector<Module::Statute> list = repoStatute.getItems();
    LONGS_EQUAL(4, list.size());
    STRCMP_EQUAL("Statute 20", list.at(0).getTitle().c_str());
    STRCMP_EQUAL("Statute 40", list.at(1).getTitle().c_str());
    STRCMP_EQUAL("Statute 60", list.at(2).getTitle().c_str());
    STRCMP_EQUAL("Archer", list.at(3).getTitle().c_str());
    repoStatute.deleteItem(id);
    // Is available with getitem it is in removed state;
    auto chkstatute2 = repoStatute.getItem(id);
    STRCMP_EQUAL("Archer", chkstatute2.getTitle().c_str());
    // Get list of the items that exist, no removals have been made
    list = repoStatute.getItems();
    LONGS_EQUAL(3, list.size());
    STRCMP_EQUAL("Statute 20", list.at(0).getTitle().c_str());
    STRCMP_EQUAL("Statute 40", list.at(1).getTitle().c_str());
    STRCMP_EQUAL("Statute 60", list.at(2).getTitle().c_str());
    // Now add item with the same id can undelete the item in repository and add again to DB
    repoStatute.addItem(id, chkstatute2);
    list = repoStatute.getItems();
    LONGS_EQUAL(4, list.size());
    STRCMP_EQUAL("Statute 20", list.at(0).getTitle().c_str());
    STRCMP_EQUAL("Statute 40", list.at(1).getTitle().c_str());
    STRCMP_EQUAL("Statute 60", list.at(2).getTitle().c_str());
    STRCMP_EQUAL("Archer", list.at(3).getTitle().c_str());

}

TEST(RepositoryAccessor, UpdateItemWithKeyAccessor) {
    for (int i = 0; i < 3; ++i) {// 20, 40, 60
		repoStatute.addItem(statutes.at(i).getStatuteID(), statutes.at(i));
    }
    LONGS_EQUAL(3, repoStatute.size());
    STRCMP_EQUAL("Statute 20", repoStatute.getItem(20).getTitle().c_str());

    // Tracking changes means key can be reset with values
    repoStatute.trackStateChanges(true);
    try {
    	repoStatute.addItem(80, Module::Statute(80, "Statute 80"));
    } catch (Repository::RepositoryException& e) {
        STRCMP_EQUAL("Repository addItem new pair KeyType and item not allowed under tracking state mode", e.what());
    }
    LONGS_EQUAL(3, repoStatute.size());

    Module::Statute newstatute(0, "Archer");
    int id = repoStatute.addItem(newstatute);
    auto chkstatute = repoStatute.getItem(id);
    STRCMP_EQUAL("Archer", chkstatute.getTitle().c_str());
    chkstatute.setTitle("Pikeman");
    repoStatute.updateItem(id, chkstatute);
    auto chkstatute2 = repoStatute.getItem(id);
    STRCMP_EQUAL("Pikeman", chkstatute2.getTitle().c_str());

}


//-------------------------------------------------------
// RepositoryTorAndAssign test group
//-------------------------------------------------------

TEST_GROUP(RepositoryTorAndAssign) {
	std::string desc{"Module DBStorage 1.0 - September 6, 2016"};
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};
    void setup() {
        dbStorage.loginUser("TEST", "pass");
        CHECK(true == dbStorage.isLoggedUser());
    }
};

TEST(RepositoryTorAndAssign, Constructor) {
    Repository::Repository<std::string> list(&dbStorage, "Key");
    LONGS_EQUAL(0, list.size());
    CHECK(list.empty());
}

TEST(RepositoryTorAndAssign, Deconstructor) {
    Repository::Repository<std::string> * list = new Repository::Repository<std::string>(&dbStorage, "ID");
    CHECK(list->empty());
    delete list;
}

//TEST(RepositoryTorAndAssign, CopyconsTor) {
//    Repository::Repository<std::string> list(&dbStorage, "Key");
//    Repository::Repository<std::string> list_copy(list);
//    LONGS_EQUAL(0, list_copy.size());
//    CHECK(list_copy.empty());
//}
