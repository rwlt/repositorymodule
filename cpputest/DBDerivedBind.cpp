
#include "DBDerivedBind.h"
#include "Statute.h"
#include <iostream>

DBDerivedBind::DBDerivedBind(std::vector<Module::Statute> *dest) :
    destSource(dest) {};
DBDerivedBind::~DBDerivedBind() {};
//
//------------------------------------------------------------
// doBind - when an item is add or set with data - flow to dest source
//------------------------------------------------------------
//
void DBDerivedBind::doBind(const Storage::RowItem &item)
{
    int id;
    std::string name;
    bool is_set;
    item.getField(1, id, is_set);
    item.getField(2, name, is_set);
    Module::Statute h(id, name);
    destSource->push_back(h); // We add the items as new when source binds to dest
};
void DBDerivedBind::doBindUpdate(const Storage::RowItem &item, int index)
{
    int id;
    std::string name;
    bool is_set;
    item.getField(1, id, is_set);
    item.getField(2, name, is_set);
    Module::Statute h(id, name);
    destSource->at(index) = h; // We add the items as new when source binds to dest
};
void DBDerivedBind::doBindClear(const Storage::RowItem &/*item*/, int index)
{
    Module::Statute h(0, "");
    destSource->at(index) = h;
};
//
//------------------------------------------------------------
// doUpdate - update an item from dest to source (BindSource items)
// Prepare a ProgRecord ready to write
//------------------------------------------------------------
//
void DBDerivedBind::doUpdate(Storage::RowItem &item, int index)
{
    if (index < 0) {
        return;
    }
    Module::Statute h = destSource->at(index);
    item.setField(1, h.getStatuteID());
    item.setField(2, h.getTitle());
};


DBDerivedItemBind::DBDerivedItemBind(Module::Statute *dest) :
    destSource(dest) {};
DBDerivedItemBind::~DBDerivedItemBind() {};
//
//------------------------------------------------------------
// doBind - when an item is add or set with data - flow to dest source
//------------------------------------------------------------
//
void DBDerivedItemBind::doBind(const Storage::RowItem &item)
{
    int id;
    std::string name;
    bool is_set;
    item.getField(1, id, is_set);
    item.getField(2, name, is_set);
    Module::Statute h(id, name);
    *destSource = h; // We add the items as new when source binds to dest
};
void DBDerivedItemBind::doBindUpdate(const Storage::RowItem &item, int /*index*/)
{
    int id;
    std::string name;
    bool is_set;
    item.getField(1, id, is_set);
    item.getField(2, name, is_set);
    Module::Statute h(id, name);
    *destSource = h; // We add the items as new when source binds to dest
};
void DBDerivedItemBind::doBindClear(const Storage::RowItem &/*item*/, int /*index*/)
{
    Module::Statute h(0, "");
    *destSource = h;
};
//
//------------------------------------------------------------
// doUpdate - update an item from dest to source (BindSource items)
// Prepare a ProgRecord ready to write
//------------------------------------------------------------
//
void DBDerivedItemBind::doUpdate(Storage::RowItem &item, int )
{
    Module::Statute &h = *destSource;
    //  std::cout << "DBDERIVED: " <<  h.getID() << " " << h.getStatute() << std::endl;
    item.setField(1, h.getStatuteID());
    item.setField(2, h.getTitle());
};

