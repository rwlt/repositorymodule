#include <unittest++/UnitTest++.h>

#include "../src/repository/BindNotify.h"

using namespace Control;

SUITE(BindNotifyAccessor)
{
    struct BindNotifyFixture {
        BindNotifyFixture()
            : action(Control::BindAction::Added),
              index(0) {
        }
        const Control::BindAction action;
        const int index;
    };

    TEST_FIXTURE(BindNotifyFixture, TextBindNotifyAction) {
        BindNotify notify;

        CHECK_EQUAL(action, notify.getAction()); // default action is Added
    }

    TEST_FIXTURE(BindNotifyFixture, TextBindNotifyIndex) {
        BindNotify notify;

        CHECK_EQUAL(index, notify.getIndex());   // default action is Added
    }
}


SUITE(BindNotifyOperator)
{
    struct BindNotifyOperatorFixture {
        BindNotifyOperatorFixture()
            : actionAdd(Control::BindAction::Added),
              actionUpdated(Control::BindAction::Updated),
              index0(0),
              index1(1) {
        }
        const Control::BindAction actionAdd;
        const int index0;
        const Control::BindAction actionUpdated;
        const int index1;
    };


    TEST_FIXTURE(BindNotifyOperatorFixture, TextBindNotifyEqual) {
        BindNotify notifyAdd;
        BindNotify notifyUpdated;
        notifyUpdated.setAction(actionUpdated);
        notifyUpdated.setIndex(index1);

        CHECK(notifyAdd == notifyAdd);
        CHECK(notifyUpdated == notifyUpdated);
    }

    TEST_FIXTURE(BindNotifyOperatorFixture, TextBindNotifyNotEqual) {
        BindNotify notifyAdd;
        BindNotify notifyAdd1;
        BindNotify notifyUpdated;
        BindNotify notifyUpdated0;
        notifyUpdated.setAction(actionUpdated);
        notifyUpdated.setIndex(index1);
        notifyUpdated0.setAction(actionUpdated);
        notifyUpdated0.setIndex(index0);
        notifyUpdated0.setAction(actionUpdated);
        notifyAdd1.setIndex(index1);

        CHECK_EQUAL(false, notifyAdd != notifyAdd);
        CHECK(notifyAdd != notifyUpdated);
        CHECK(notifyUpdated0 != notifyUpdated);
        CHECK(notifyAdd1 != notifyUpdated0);
    }

    TEST_FIXTURE(BindNotifyOperatorFixture, BindNotifyPerformance) {
        UNITTEST_TIME_CONSTRAINT(10);
        BindNotify notifyAdd;
        BindNotify notifyAdd1;
        BindNotify notifyUpdated;
        BindNotify notifyUpdated0;
        notifyUpdated.setAction(actionUpdated);
        notifyUpdated.setIndex(index1);
        notifyUpdated0.setAction(actionUpdated);
        notifyUpdated0.setIndex(index0);
        notifyUpdated0.setAction(actionUpdated);
        notifyAdd1.setIndex(index1);

        CHECK(notifyAdd == notifyAdd);
        CHECK(notifyAdd != notifyUpdated);
        CHECK(notifyUpdated0 != notifyUpdated);
        CHECK(notifyAdd1 != notifyUpdated0);

        //UnitTest::TimeHelpers::SleepMs (50) ; // Going to run out of time !
    }
}
