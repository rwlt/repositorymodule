#ifndef REPOSITORY_EXCEPTION_H_
#define REPOSITORY_EXCEPTION_H_

#include <exception>
#include <string>

namespace Repository
{

/// Standard exception override
class RepositoryException: public std::exception
{
public:
    /// Constructor
    RepositoryException ( std::string what ) :reason ( what ) {
    }
    /// Virtual Deconstructor
    virtual ~RepositoryException() {
    }
    /// What went wrong
    const char *what() const _GLIBCXX_USE_NOEXCEPT {
        return ( reason.c_str() );
    }

private:
    std::string reason;

};

} /* namespace Repository */

#endif /* REPOSITORY_EXCEPTION_H_ */
