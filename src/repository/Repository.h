#ifndef RWLTREPOSITORY_H_
#define RWLTREPOSITORY_H_

#include <vector>
#include <algorithm>
#include <tuple>
#include <mutex>
#include <unordered_map>
#include <map>
#include "RepositoryException.h"
#include "storage/DBStorage.h"

namespace Repository {

/** \page bindmodule Introduction to Bind Module Introduction
 * This page introduces the user to the topic.
 */

/// Repository Item state
/**
 * If added or changed it is always used to populate views.
 * If added or changed at any time item after initial adding its state is changed to dirty.
 * If deleted it is not removed but set to dirty and not used to populate view.
 */
struct RepositoryItemState {
	bool exists{true};    ///< means a new record item is added
	bool removed{false};  ///< it removed as is existed persistence source - it can be undone to added state
};


/// A template collection of items using a DataType
/**
 * Uses RepositoryItemState to mark item exists or removed.
 * The initial adding of items does get exists state. Until TrackStateChanges. When tracking
 * state added item can be used to undelete remove state on items. Ie. use same key to add the removed item.
 * This list can be retrieved to get all existi state items.
 * \tparam DataType items in this collection
 * \tparam KeyType keys of items in this collection
 */

template<class DataType, typename KeyType = int>
class Repository {

	typedef std::tuple<RepositoryItemState, DataType> RepositoryItem;
	std::map<KeyType, RepositoryItem> m_container;
	typedef std::pair<KeyType, RepositoryItem> RepoItemPair;
	bool m_isTrackingItemState{false}; ///<
	Storage::DBStorage * const m_dbStorage;
	std::string m_keyField{""};
	mutable std::mutex m_Mutex{};

public:
	/// Public constructor
	/**
	 * \param dbStorage ref to DbStorage
	 * \param keyField string of the Key field for the DataType
	 */
	Repository(Storage::DBStorage * const dbStorage, const std::string &keyField):
		m_dbStorage(dbStorage), m_keyField(keyField) {
	}
	/// Public deconstructor

	virtual ~Repository() {
	}

	/// trackChanges
	/**
	 * When accept is false (default state) items added are mark with added state non dirty
	 * so we know they exist in a persistence source
	 * When accept is true items added are marked as added with isDirty and need to be persisted to DB source
	 * \param accept bool
	 */
	void trackStateChanges(bool accept) {
		std::lock_guard<std::mutex> lock(m_Mutex);
		m_isTrackingItemState = accept;
	}
	/// Get Key Field name
	const std::string& getKeyField() {
		return m_keyField;
	}
	/// Is it tracking
	const bool& isTrackingState() {
		return m_isTrackingItemState;
	}
	/// AddItem
	/**
	 * \param key KeyType
	 * \param item a DataType item
	 */
	virtual void addItem(KeyType key, const DataType& item) {
		if (m_container.find(key) != m_container.end()) {
			if (m_isTrackingItemState) {
				existsStateChange(key, item);
				return;
			} else {
				throw (RepositoryException("Repository addItem no duplicate key can be added"));
			}
		}
		if (m_isTrackingItemState) {
			// New items can't be added under tracking state as it needs to update
			// DBstorage so addItem with no keytype is used for this method
			throw (RepositoryException("Repository addItem new pair KeyType and item not allowed under tracking state mode"));
		}
		// New Key and Data to add
		RepositoryItemState state; // initial state is exists
		RepositoryItem repoTitem(state, item);
		RepoItemPair addItemPair = std::make_pair(key, repoTitem);
	    {
			std::lock_guard<std::mutex> lock(m_Mutex);
			m_container.insert(addItemPair);
		}


	}
	/// AddItem
	/**
	 * When added it will notify added and the index added at.
	 * \param item a DataType item
	 * \return KeyType the new key made for this item under trackingstate mode - from DB
	 */
	KeyType addItem(const DataType& item) {
		if (!m_isTrackingItemState) {
			throw (RepositoryException("Repository addItem not in tracking state mode to add new item without the KeyType"));
		}
		// New Key and Data to add
		// Use DBStorage to add item and get the new key for KeyType value
		Storage::EntityMap<DataType> tent;
		KeyType key = 0;
		tent.getField(item, m_keyField, key);
		if (m_container.find(key) != m_container.end()) {
			addItem(key, item);
			return key;
		}
		try {
			DataType new_item = m_dbStorage->doAddItem<DataType>(item);
			tent.getField(new_item, m_keyField, key);
			RepositoryItemState state; // initial state is exists
			RepositoryItem repoTitem(state, new_item);
			RepoItemPair addItemPair = std::make_pair(key, repoTitem);
		    {
				std::lock_guard<std::mutex> lock(m_Mutex);
				m_container.insert(addItemPair);
			}
		} catch (Storage::StorageException &e) {
			throw (RepositoryException("Repository addItem " + std::string(e.what())));
		}
		std::cout << "Repo addItem =>exists: " << key << "\n";
		return key;
	}
	/// Delete item
	/**
	 * \param key KeyType
	 */
	void deleteItem(KeyType key) {
		if (m_container.find(key) != m_container.end()) {
			if (m_isTrackingItemState) {
				removeStateChange(key);
				return;
			} else {
				throw (RepositoryException("Repository deleteItem available in tracking state mode only"));
			}
		}
	}

	/// Change or update item
	/**
	 * \param key KeyType
	 * \param item DataType
	 */
	void updateItem(KeyType key, const DataType& item) {
		if (m_container.find(key) != m_container.end()) {
			if (m_isTrackingItemState) {
				existsStateChange(key, item);
				return;
			} else {
				throw (RepositoryException("Repository updateItem available in tracking state mode only"));
			}
		} else {
			throw (RepositoryException("Repository updateItem key not found"));
		}
	}

	/// Get item with a Key
	/**
	 * \param key KeyType
	 * \return DataType referenced
	 */
	virtual const DataType &getItem(KeyType key)
	{
		if (m_container.find(key) == m_container.end()) {
			throw (RepositoryException("Repository::getItem not found"));
		}
		RepositoryItem& repoItem = m_container.at(key);
		return (std::get<1>(repoItem));
	}
	/// Get vector of items in the repo active for select list
	/**
	 * Uses RepositoryItemState to get all add/updated items
	 *
	 * \return std::vector of DataType
	 */
	virtual std::vector<DataType> getItems()
	{
		std::lock_guard<std::mutex> lock(m_Mutex);
		std::vector<DataType> list;
		for (RepoItemPair pair: m_container) {
			RepositoryItemState state;
			DataType item;
			std::tie(state, item) = pair.second;
			if (state.exists) {
				list.push_back(item);
			}
		}
		return (list);
	}
	/// Clear and collection

	/**
	 * When clear it will notify clear and the index at.
	 *
	 */
	virtual void clear() {
		std::lock_guard<std::mutex> lock(m_Mutex);
		m_container.clear();
	}
	/// The size of collection

	virtual unsigned int size() {
		return (m_container.size());
	}
	/// The size of collection

	virtual bool empty() {
		return (m_container.empty());
	}

private:
	/// removeStateChange
	void removeStateChange(const KeyType &key) {
	    {
			std::lock_guard<std::mutex> lock(m_Mutex);
			RepositoryItem& repoItem = m_container.at(key);
			RepositoryItemState& state = std::get<0>(repoItem);
			if (state.exists == true) {
				DataType& titem = std::get<1>(repoItem);
				//Use DBStorage to delete this Key
				try {
					m_dbStorage->doRemoveItem<DataType>(titem);
					std::cout << "RepoStateChange remove: " << key << "\n";
				} catch (Storage::StorageException &e) {
					throw (RepositoryException("Repository addItem " + std::string(e.what())));
				}

			}
			state.exists = false;
			state.removed = true;
	    }
	}

	/// removeStateChange
	void existsStateChange(const KeyType &key, const DataType &item) {
	    {
			std::lock_guard<std::mutex> lock(m_Mutex);
			RepositoryItem& repoItem = m_container.at(key);
			RepositoryItemState& state = std::get<0>(repoItem);
			try {
				if (state.removed == true) {
					//Add item with same key with DBStorage
					m_dbStorage->doAddItem<DataType>(item);
					std::cout << "RepoStateChange remove=>exists: " << key << "\n";
				} else {
					//Use repoItem as original and item to update DBStorage
					DataType &titem = std::get<1>(repoItem);
					m_dbStorage->doUpdateItem<DataType>(titem, item);
				}
			} catch (Storage::StorageException &e) {
				throw (RepositoryException("Repository addItem " + std::string(e.what())));
			}

			state.exists = true;
			state.removed = false;
			DataType& titem = std::get<1>(repoItem);
			titem = item;
	    }
	}

};

} /* namespace Repository */

#endif /* RWLTREPOSITORY_H_ */
